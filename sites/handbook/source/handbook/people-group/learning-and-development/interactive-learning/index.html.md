---
layout: handbook-page-toc
title: "Handbook-First Approach to Interactive Learning"
description: "All elearning courses tied to the Field Certification Program will utilize the Handbook as the Single Source of Truth (SSOT)"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

All elearning courses at GitLab to the will utilize the Handbook as the Single Source of Truth (SSOT). 

![gitlab-learning-content-diagram](/handbook/sales/field-operations/field-enablement/interactive-learning/gitlab-learning-content-diagram.png)

### Handbook-First Approach to Interactive Learning in Action

#### Field Enablement 
The GitLab Field Enablement team uses a handbook-first approach to learning in their [Field Certification Program](/handbook/sales/training/field-certification/) 

For example, in order to learn more about a particular topic tied to the Field Certification, learners will be able to refer to the Handbook within the elearning course instead of having to open a new browser tab and manually navigating to the Handbook.

The Field Enablement team will utilize the Articulate 360 Storyline Suite as the main course authoring tool. In Storyline, a [Handbook-first approach](/company/culture/all-remote/handbook-first-documentation/#gitlab-knowledge-assessment-handbook-first-documentation) is achieved by adding a web object on a slide so learners can access the Handbook on that particular topic.


#### Learning and Development

The L&D team hosts [Mini and Extended Challanges](https://about.gitlab.com/handbook/people-group/learning-and-development/#gitlab-mini-and-extended-challenges) to engage team members in bite sized learning about a specific topic. Participants in the challenge collaborate on issues and are driven to the handbook to review learning content.


## Interactivity in E-learning
Interactivity such as games, drag-and-drop, matching, and other interactive components in a published elearning course will not be accessible via the Handbook; however, all information conveyed to learners through the elearning course will have an associated Handbook page as needed. This will make sure that despite the interactivity of the elearning course, learners will have access to all topics covered in the Field Certification program via the Handbook.

## Course Development Workflow
Instructional designers will use the ADDIE instructional systems design (ISD) framework to design and develop Field Certification courses using Articulate 360. The workflow of course development will be as follows:
1. Create learning objectives
1. Design storyboard and get SME buy in
1. Outline course Handbook page 
1. Develop elearning
1. Conduct a course Alpha
1. Incorporate feedback
1. Conduct a course Beta (as needed)
1. Publish course in LXP
1. Announce course launch
1. Enroll learners (as needed)

## Handbook Course Documentation
A thorough needs assessment will be conducted to determine what net-new Handbook pages are needed as new courses are developed. The goal is for each course to have an associated Handbook page that will include learning objectives, static learning content, quizzes, and applicable links. 

## Handbook-First Learning in the EdCast LXP

Content in the EdCast LXP at GitLab will adopt this handbook-first approach to learning. Examples of ways that the handbook is utilized for learning in the LXP include:

- SmartCards linking to specific handbook pages
- Pathways leading learners through related handbook pages to train on a specific skill or content area
- iFrames displaying content from the handbook directly in the LXP
- Assessments and learning checks referring back to content in the handbook to check for understanding

