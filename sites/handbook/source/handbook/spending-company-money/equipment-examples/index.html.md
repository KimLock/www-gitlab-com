---
layout: handbook-page-toc
title: "Spending Company Money - Equipment Examples"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Adapters and cables

#### A note on HDMI and 4K@60Hz

If you need HDMI connectivity (e.g. to connect a TV, projector or monitor) and are looking for a USB-C adapter or hub that supports [4K resolution](https://en.wikipedia.org/wiki/4K_resolution) (3840 × 2160 pixels), be aware that most of the adapters and hubs on the market only support refresh rate of 30 Hz (i.e. 30 frames per second). This is ok for movies, but not great for interactive work. For example, mouse pointer will noticeably jump on a 30 Hz screen when you move it. Scrolling text is even worse. All that puts extra strain on your eyes. There are products that support refresh rate of 60 Hz and they cost roughly the same. Look for 4K@60Hz in product specification. If you bought one and it still does not give you the desired refresh rate, make sure your HDMI cable supports it. [HDMI 2.0](https://en.wikipedia.org/wiki/HDMI#Version_2.0) and newer cables do.

#### USB adapters

  * Falwedi 7-in-1 USB-C Hub - [US](https://www.amazon.com/dp/B083FBYP9B), [Australia](https://www.amazon.com.au/dp/B083FBYP9B). HDMI port supports 4K@60Hz.
  * TOTU 8-in-1 USB-C Hub - [US](https://www.amazon.com/dp/B07FX2LW35/)
  * FLYLAND Hub, 9-in-1 - [Germany](https://www.amazon.de/dp/B00OJY12BY/)
  * VAVA USB-C Hub 8-in-1 Adapter - [Australia](https://www.amazon.com.au/dp/B07JCKCZGJ/)
  * UGREEN Ethernet to USB 3.0 Adapter - [US](https://www.amazon.com/dp/B00MYTSN18/)
  * Yinboti USB-C Hub for New Macbook Pros - [US](https://www.amazon.com/gp/product/B07FMNJC6J/)
  * Kensington UH4000 4 Port USB Hub 3.0 - [US](https://www.amazon.com/Kensington-UH4000-Port-USB-3-0/dp/B00O9RPP28/)
  * YXwin USB C Hub 6-in-1 Adapter including Ethernet - [UK](https://www.amazon.co.uk/YXwin-Adapter-Delivery-1000mbps-Ethernet/dp/B07PSM6RQS/)
  * HyperDrive 4 in 1 USB C Adapter - [India](https://www.amazon.in/HyperDrive-Thunderbolt-Macbook-MacBook-Devices/dp/B01M7O0WF4/)
  * UGREEN USB-C Hub 9-in-1 - [US](https://www.amazon.com/UGREEN-Multiport-Ethernet-Charging-Compatible/dp/B076HN81GS/), [ethernet-to-usb driver](https://www.asix.com.tw/products.php?op=pItemdetail&PItemID=131;71;112&PLine=71)

#### USB Docks
  * CalDigit TS3 Plus (**Will require Manager Approval to expense due to cost**) - [US](http://shop.caldigit.com/us/index.php?route=product/product&product_id=170), [UK](http://shop.caldigit.com/uk/index.php?route=product/product&product_id=174)
    * Enables *stable* Dual Monitor Support for Engineers
      * Extended Desktop Support for DVI monitors requires ['active' displayport adaptors](http://www.cablematters.com/pc-33-33-cable-matters-gold-plated-displayport-to-dvi-male-to-female-adapter.aspx)
      * macOS does not support [Multi-Stream Transport over DisplayPort](https://www.displayport.org/cables/driving-multiple-displays-from-a-single-displayport-output/)
    * Recharges Laptop over USB-C
    * Provides USB-A support for peripherals

#### Cables
  * AmazonBasics Premium HDMI Cable - [US](https://www.amazon.com/dp/B07KSD9DZ9/) - Supports 4K@60Hz
  * Rankie DisplayPort Cable - [UK](https://www.amazon.co.uk/gp/product/B00YOP0T7G/)
  * CHOETECH USB-C to DisplayPort Cable - [India](https://www.amazon.in/CHOETECH-DisplayPort-Thunderbolt-Compatible-2016-2020/dp/B08FBJ8DD3) - Supports 4K@60Hz

#### Network Adaptors
  * TP-Link Powerline Adapter - [US](https://www.amazon.com/TP-LINK-Powerline-Pass-Through-TL-PA9020P-KIT/dp/B01H74VKZU), [UK](https://www.amazon.co.uk/TP-Link-TL-PA8033PKIT-Gigabit-Passthrough-Powerline/dp/B07GFHQXBP) - You'll want to aim for adaptors that can support the max ethernet standard or more of 1000mbps.

### Notebook carrying bags
  * tomtoc 360° Protective Sleeve - [US](https://www.amazon.com/dp/B01N0TOQEO/)
  * NIDOO 15" - [Germany](https://www.amazon.de/dp/B072LVYC91/)
  * Mosiso Sleeve - [Australia](https://www.amazon.com.au/dp/B01N0W1YIK/)
  * SLOTRA Slim Anti-Theft Laptop Backpack - [UK](https://www.amazon.co.uk/SLOTRA-Lightweight-Resistant-Multipurpose-Anti-Theft/dp/B01DKLOOLG)

### Monitors

#### Desktop monitors
  * 4K monitors:
    * LG 27UD58-B 27" 4K - [US](https://www.amazon.com/dp/B01IRQAYPE/)
    * LG 27UK850-W 27" 4K UHD with USB Type-C - [US](https://www.amazon.com/dp/B078GVTD9N/)
    * LG 27UL650-W 27" 4K UHD LED - [US](https://www.amazon.com/LG-27UL650-W-Monitor-DisplayHDR-White/dp/B07MKT2BNB)
    * BenQ PD2700U 27" 4K UHD IPS  - [US](https://www.amazon.com/BenQ-PD2700U-Professional-Monitor-3840x2160/dp/B07H9XP92N)
    * LG 27UL500W 27" 4K UHD IPS - [India](https://www.amazon.in/LG-4K-UHD-Monitor-Display-Freesync/dp/B07PGL2WVS/)
  * Dell P2418D 23.8" IPS QHD - [UK](https://www.amazon.co.uk/DELL-P2418D-23-8-Inch-Widescreen-Monitor/dp/B074MMR1V3)
  * Dell Ultra Sharp LED-Lit Monitor 25" 2560 X 1440 60 Hz IPS - [US](https://www.amazon.com/Dell-LED-Lit-Monitor-U2518D-Compatibility/dp/B075KGLYRL)
  * Acer S242HLDBID 24" - [Germany](https://www.amazon.de/dp/B01AJTVCA8/)
  * ASUS VS248H-P 24" 1080p - [US](https://www.amazon.com/dp/B0058UUR6E/)
  * ASUS PB277Q 27" 1440p - [US](https://www.amazon.com/gp/product/B01EN3Z7QQ/)
  * SAMSUNG F350 23.6" 1080p - [Germany](https://www.amazon.com.au/dp/B0771J3HXV/)
  * Lenovo ThinkVision P27h-10 27" 1440p - [Switzerland](https://www.digitec.ch/de/s1/product/lenovo-thinkvision-p27h-10-27-2560-x-1440-pixels-monitor-6611407)
    * Connects over USB-C and also acts as a hub with 4 USB3.0 ports (on the back), works great on Linux including audio passthrough!

#### Portable monitors
  * USB Touchscreen, 11.6" - [US](https://www.amazon.com/dp/B07FKJ6WP1/)
  * Kenowa 15.6" - [Germany](https://www.amazon.de/dp/B07FZ5PNDV/)
  * Asus Zenscreen 15,6" - [Netherlands](https://www.coolblue.nl/product/787645/asus-zenscreen-mb16ac.html)
  * Lepow 15.6" - [US](https://www.amazon.com/dp/B07RGPCQG1)
  * Duet App for iPad as a second monitor - [App store](https://apps.apple.com/app/duet-display/id935754064) and [Mac/Windows install](https://www.duetdisplay.com/)

#### Privacy screens
  * Macbook Pro 15" - [US](https://www.amazon.com/gp/product/B07GV71FF5/)
  * Macbook Pro 13" - [US](https://www.amazon.com/gp/product/B07GV71FF5/)

### Headphones and earbuds
  * Mpow 059 Bluetooth Over Ear Headphones - [US](https://www.amazon.com/dp/B077XT82DD/)
  * JBL T450BT On-ear Bluetooth Headphones - [Germany](https://www.amazon.de/dp/B01M6WNWR6/)
  * Apple AirPods are not recommended - [US](https://www.apple.com/shop/accessories/all-accessories/headphones-speakers)
    * Note: It is sometimes difficult to hear team members who are using the microphone on AirPods. [This may be due to AirPods automatically switching the microphones between left and right](https://authenticstorytelling.net/how-to-fix-your-airpods-microphone-to-work-better-on-phone-calls-and-not-cut-out/). This can be fixed by [setting the AirPods to use the audio mic from one or the other](https://www.pcmag.com/how-to/how-to-connect-your-airpods-to-your-mac). Another option is to use an [external microphone](https://about.gitlab.com/company/culture/all-remote/workspace/#microphones). AirPods also have limited battery life, and we often see problems with them not lasting through multiple meetings. Consider options that will last longer and not cause interruptions.

Keep in mind, open-ear headphones can often be worn longer than in-ear or closed headphones.

### Webcams
  * Logitech C920 - [US](https://www.amazon.com/Logitech-Widescreen-Calling-Recording-Desktop/dp/B006JH8T3S), [UK](https://www.amazon.co.uk/Logitech-C920-Pro-Webcam-Recording/dp/B006A2Q81M?)
  * Logitech C525 - [India](https://www.amazon.in/Logitech-C525-HD-Webcam-Black/dp/B008QS9MRA)

### Keyboards
  * Macally Bluetooth wireless keyboard - [US](https://www.amazon.com/Macally-Bluetooth-Computers-Rechargeable-Indicators/dp/B07K24ZLWC)
  * Logitech Ergo K860 Wireless Ergonomic Keyboard with Wrist Rest [US](https://www.amazon.com/gp/product/B07ZWK2TQT/ref=ppx_yo_dt_b_asin_title_o06_s00?ie=UTF8&psc=1)

### U2F Tokens 
To understand more about where these can be used see the [Security Practices Page](https://about.gitlab.com/handbook/security/#two-factor-authentication).
  * Google Titan Security Key - [AT, CA, FR, DE, IT, JA, ES, CH, UK, US (excl. PR)](https://store.google.com/product/titan_security_key)
  * Yubikey - [Ships to most countries](https://www.yubico.com/store/)


### Office Furniture

#### Desks
  * Autonomous SmartDesk 2 - Home Edition - [US and Europe](https://www.autonomous.ai/standing-desks/smart-desk-2-home)
  * Jarvis electric adjustable height desk - [US and Canada](https://www.fully.com/standing-desks/jarvis.html), [Europe](https://www.fully.eu/pages/jarvis-adjustable-standing-desks)

#### Chairs
  * Hbada Ergonomic Office Chair - [US](https://www.amazon.com/dp/B01N0XPBB3/)
  * INTEY Ergonomic Office Chair - [Germany](https://www.amazon.de/dp/B0744GS6LR/)
  * Kolina Ergonic Mesh Office Chair - [Australia](https://www.amazon.com.au/dp/B07BK7XDV8/)
  * IKEA MARKUS Office Chair - [UK](https://www.ikea.com/gb/en/products/chairs-stools-benches/desk-chairs/markus-office-chair-glose-black-art-20103101)
  * Featherlite Liberate Medium Back Desk Arm Chair - [India](https://www.amazon.in/Featherlite-Liberate-Medium-Chair-Black/dp/B07FPD46L3)

### Other Accessories

#### Laptop Stands
  * BoYata Adjustable Laptop Stand - [UK](https://www.amazon.co.uk/gp/product/B07H89V3BB)
  * Roost Laptop Stand - [Worldwide](https://www.therooststand.com/)
    * If you are based in Australia, it is cheaper and faster to get it from the [Australian store.](https://www.therooststand.com.au/)
  * AmazonBasics Notebook Laptop Stand Arm Mount Tray - [US](https://www.amazon.com/gp/product/B010QZD6I6)

#### Wrist Rests
  * GIM Wrist Rest Set - [UK](https://www.amazon.co.uk/Keyboard-GIM-Support-Ergonomic-Computer/dp/B072K41FC1)

#### Whiteboards
  * Audio-Visual Direct White Magnetic Glass Dry-Erase Board Set - [US](https://www.amazon.com/dp/B00JNJWE3K)

### Something else?
  * No problem! Consider adding it to this list if others can benefit from it.
