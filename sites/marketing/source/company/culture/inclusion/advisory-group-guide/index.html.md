---
layout: markdown_page
title: "Diversity, Inclusion & Belonging  Advisory Group Guidelines"
description: "This document provides team members with the information they need to understand and sustain a Diversity, Inclusion & Belonging Advisory Group."
canonical_path: "/company/culture/inclusion/advisory-group-guide/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

##  Introduction
This document provides team members with the information they need to understand and sustain a Diversity, Inclusion & Belonging  Advisory Group here at GitLab. Additionally you will be provided:

*   Introduction to Diversity, Inclusion & Belonging  Groups
*   GitLab's definition of [Diversity, Inclusion & Belonging ](/company/culture/inclusion/)
*   What is a Diversity, Inclusion & Belonging  group
*   The purpose of the global Diversity, Inclusion & Belonging group
*   Current DIB [Advisory Group Members List](https://about.gitlab.com/company/culture/inclusion/advisory-group-members/)


### Purpose and Benefits of a DIB Advisory Group
Internal diversity groups are a valuable component of a company’s diversity, inclusion, and belonging strategy, because they provide an inclusive and effective mechanism for managing and initiating diversity, inclusion, and belonging programs. Diversity councils/groups successfully integrate the company’s DIB programs with their operations, strategies, and missions and objectives. They provide platforms for assessing the effectiveness of the DIB program, introducing reform, and overseeing the DIB.  They also demonstrate the companies’ commitment to DIB.

The Global DIB Advisory Group is designed to provide greater representation of the diversity, inclusion and belonging of GitLabs workforce.  The group will assist with global feedback in implementing the diversity, inclusion and belonging strategy, policies and initiatives.

### Responsibilities of the Group
*   Align GitLabs organization and programs with business needs, practices, and strategies
*   Have a long-term strategy, stated purposes, goals, and objectives, and a mission statement 
*   Work closely with executive and operational leadership, diversity and people ops leadership, staff, and organizations (especially employee resource groups) 
*   Partner with senior leadership for support and participation 
*   Demonstrate the importance of diversity to the success of the enterprise 
*   Communicate the goals and objectives, program efforts, and successes consistently and repeatedly to GitLab and its workforce 
*   Secure a reputation as a source of good counsel, support, and understanding
*   Each member should commit to serving a minimum of 1 year in the group

#### Items that the group is not Responsible for
Items that are specific to a [Team Member Resource Group (TMRG)](https://about.gitlab.com/company/culture/inclusion/erg-guide/) should be passed to the TMRG (for example, the [women's TMRG](https://about.gitlab.com/company/culture/inclusion/tmrg-gitlab-women/) may wish to handle the Grace Hopper Celebration planning).

### Roles Within the Group

#### Advisory Group Members 

At GitLab we all contribute!  Everyone has an opportunity to lead and provide feedback within the group. The group will be refreshed each October/November. Members will be asked to confirm participation, group size and membership will be evaluated to confirm we are sufficiently diverse to represent the needs of team members at GitLab.

#### Executive Sponsor  
An executive GitLab team member who is responsible and accountable for strategic support of the group

*   Share accountability for the success of the DIB group
*   Participate as an active member of the DIB group
*   Share information about the DIB group activities with other leaders
*   Provide insight and guidance to DIB group as needed
*   Partner with the DIB group/council Lead and Co-Lead on issues, concerns, and resource needs of the community
*   May provide additional budget

#### Lead
A GitLab team member who is responsible and accountable for strategic direction and operations of the Global DIB Advisory Group

*   Operational lead of the Global DIB Advisory Group
*   Meets w/ DIB Partner regularly
*   Responsible for submitting annual and quarterly plans
*   Along with Vice-chair, serves as contact for any team, department, or other GitLab team member requesting partnership or education with the Global DIB Advisory Group


#### Co-Lead
A GitLab team member who supports the Lead and Sponsor in the strategy and operations of the Global DIB Advisory Group

*   Serve as lead in the absence of the Chair
*   Along with Chair, serves as contact for any team, department, or other GitLab team member requesting partnership with the DIB group

#### Project Groups
A GitLab team member who is responsible and accountable for specific deliverables of the Global DIB Advisory Group

*   For example, members who volunteer to be part of the newletter group.

### Time Commitments
A GitLab team member who is a member of the Advisory Group should expect to spend time on the following items. There are additional commitments required if a member chooses to take on an additional role.

* Attend the monthly meeting
* Perform actions you volunteer for. Potential board member actions are 
    * Making an MR and moving it forward to get company/community contribution and get it merged. 
    * Gatehring content for a newsletter.
    * Promoting DIB events and initatives 
    * etc. 
