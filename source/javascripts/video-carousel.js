$('.video-slider').slick({
  dots: false,
  centerMode: false,
  infinite: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 4,
  arrows: true,
  nextArrow: '<div class="slick-arrow-next"><i class="fas fa-arrow-right"></i></div>',
  prevArrow: '<div class="slick-arrow-prev"><i class="fas fa-arrow-left"></i></div>',
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});  

$('#videoCarouselModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) 
  var videoUrl = button.data('video-link') 
  var modal = $(this)
  modal.find('.modal-title').text(videoUrl);
  modal.find('.share-text').text(videoUrl);
  modal.find('.carousel-modal-iframe').attr("src", videoUrl);
})

$('.btn-close-modal').click(function(e) {
  e.preventDefault();
  $('.carousel-modal-iframe').attr('src', '');
});
